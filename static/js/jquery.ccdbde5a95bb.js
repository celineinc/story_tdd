$(document).ready(function(){
    // Change theme
    $('#change_theme').click(function() {
        $('#change_theme').css({"background-color": 'yellow', "font-family": "Lato", "color" : 'black'});
        $('body').css({"background-color": "black", "font-family": "Lato", "color":"white"});
        $('.navbar-expand-lg').css({"background-color": 'yellow', "font-family": "Lato", "color" : 'black'});    
        $('.panel-body').css({"font-family": "Lato", "color":"black"});
    });
});