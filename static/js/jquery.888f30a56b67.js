$(document).ready(function(){
    // Change theme
    $('#change_theme').click(function() {
        $('#change_theme').css({"background-color": 'yellow', "font-family": "Lato", "color" : 'black'});
        $('body').css({"background-color": "black", "font-family": "Lato", "color":"white"});
        $('.navbar-expand-lg').css({"background-color": 'yellow', "font-family": "Lato", "color" : 'black'});    
        $('.panel-body').css({"font-family": "Lato", "color":"black"});
    });
    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 3000);    
});

// Loader
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 1000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

// JSON
$('#json').click(
    function(){
        $.ajax({
            url: "http://localhost:8000/static/js/test.json",
            success: function(result){
                console.log(result);
                for(i=0; i<result.length; i++){
                    var tmp = "<tr><td>" + result[i].id + "</td><td>" + result[i].name + "</td></tr>";
                    $("table").append(tmp);

                }
            }
        });
    }
);