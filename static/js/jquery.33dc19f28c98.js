$(document).ready(function(){
    // Change theme
    $('#change_theme').click(function() {
        $('#change_theme').css({"background-color": 'yellow', "font-family": "Lato", "color" : 'black'});
        $('body').css({"background-color": "black", "font-family": "Lato", "color":"white"});
        $('.navbar-expand-lg').css({"background-color": 'yellow', "font-family": "Lato", "color" : 'black'});
    });
    // Accordion
    var panels = $('#accordion > panel-body').hide()
    $('#accordion > panel-body').click(function() {
        var $this = $(this);
        panels.slideUp();
        $this.parent().next().slideDown();
    })
});