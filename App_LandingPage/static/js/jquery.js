$(document).ready(function(){
    // Load table of books using Ajax
    $.ajax({
        url: "/json",
        success: function(result) {
            console.log("halo");
            var data = result.book_data;
            for (var i = 0; i < data.length; i++) {
                var title = data[i].title;
                var author = data[i].authors;
                var publishedDate = data[i].publishedDate;
                var id = data[i].id;
                // var button = '<button id =" '+ data[i].id + 'type="button" class="btn btn-default btn-sm">'
                //        '<span class="glyphicon glyphicon-star" id = "star"></span> Add to Favorite</button>';
                var button = '<img id="' + data[i].id + '" onclick="changeStar(id)" width="25" height="25" src="https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png">';                
                var tmp = '<tr><td>' + title + '</td>' +
                                '<td>' + author + '</td>'
                            + '<td>' + publishedDate + '</td><td>'+ button + '</td></tr>';
                $('#table_content').append(tmp);
            }
        },
        error: function(error) {
            alert("Halaman buku tidak ditemukan")
        }
    });
    
    // Change theme
    $('#change_theme').click(function() {
        $('#change_theme').css({"background-color": 'yellow', "font-family": "Lato", "color" : 'black'});
        $('body').css({"background-color": "black", "font-family": "Lato", "color":"white"});
        $('.navbar-expand-lg').css({"background-color": 'yellow', "font-family": "Lato", "color" : 'black'});    
        $('.panel-body').css({"font-family": "Lato", "color":"black"});
    });

    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 3000); 
    
    // Exercise of using Ajax
    $("#json").click(
        function(){
            console.log("halo");
            $.ajax({
                url: "http://localhost:8000/static/js/test.json",
                success: function(result){
                    console.log(result);
                    for(i=0; i<result.length; i++){
                        var tmp = "<tr><td>" + result[i].id + "</td><td>" + result[i].name + "</td></tr>";
                        $("#table").append(tmp);
                    }
                }
            });
        }
    );
});

// Loader
var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 1000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

function changeStar(id) {
    $.ajax({
        url: "/json",
        success: function(result) {
            var data = result.book_data;
            var counter = parseInt(document.getElementById('counter').innerHTML);
            for (var i = 0; i < data.length; i++) {
                var id2 = data[i].id;
                if(id == id2) {
                    var image = document.getElementById(id2);
                    if (image.src.match("https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png")) {
                        image.src = "https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/favorited.png";
                        counter++;
                    } else {
                        image.src = "https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png";
                        counter--;
                    }
                }
            }
            $('#counter').replaceWith('<span id="counter">' + counter + '</span>');
        },
        error: function(error) {
            alert("Halaman buku tidak ditemukan")
        }
    });
}

// var string;
// function Search() {
//     $.ajax({
//         string: document.getElementById('search_books').value,
//         url: "/books/" + string,
//         success: function(result) {
//             var data = result.book_data;
//             for (var i = 0; i < data.length; i++) {
//                 var title = data[i].title;
//                 var author = data[i].authors;
//                 var publishedDate = data[i].publishedDate;
//                 var id = data[i].id;
//                 var button = '<img id="' + data[i].id + '" onclick="changeStar(id)" width="25" height="25" src="https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png">';                
//                 var tmp = '<tr><td>' + title + '</td>' +
//                           '<td>' + author + '</td>' +
//                           '<td>' + publishedDate + '</td>' +
//                           '<td>'+ button + '</td></tr>';
//                 $('#table_content').append(tmp);
//             }
//         },
//         error: function(error) {
//             alert("Halaman buku tidak ditemukan")
//         }
//     });

// }

// ------------ STORY 10 ------------
// FUNGSI UNTUK MENGECEK APAKAH FORMULIR VALID DAN APAKAH EMAIL SUDAH TERDAFTAR
function checkRegistered() {
    // Mengambil semua data yang dibutuhkan dari ID nya
    var full_name = document.getElementById('full_name').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    // Mengambil csrf token yang digenerate ketika field form diisi
    var csrftoken = $('input[name = "csrfmiddlewaretoken"]').attr('value');
    $.ajax({
        // Mengakses url check-is-valid yang akan memanggil sebuah fungsi di
        // views.py dengan mengirimkan data dalam format JSON yang akan
        // diproses dengan method POST 
        url : '/check-is-valid',
        method : 'POST',
        data : JSON.stringify({'full_name' : full_name, 'email' : email, 'password' : password}),

        // Sebelum mengirim datanya, sertakan juga token yang digenerate saat
        // field form diisi supaya data yg dikirim terjamin merupakan data
        // hasil input form kita
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrftoken);
        },
        success : function(result){
            // Boolean apakah form valid dan email pernah terdaftar, yang didapat
            // dari hasil pengecekan data (yg tadi dikirim) di database pada fungsi di views.py
            let emailExist = result.email_is_exist;
            let formValid = result.form_is_valid;

            // Jika email sudah terdaftar atau form tidak valid
            if (emailExist || !formValid) {
                $('#submit').prop("disabled", true);    // button SUBMIT belum bisa diklik
                $('#form-info').prop("hidden", false);  // error message akan dimunculkan

                if (!formValid) {
                    $('#form-info').text("Form not valid!");        // error message jika form tidak valid
                } else if (emailExist) {
                    $('#form-info').text("Email already exists!");  // error message jika email sudah terdaftar
                }
            // Jika email belum pernah terdaftar dan form valid
            } else {
                $('#submit').prop("disabled", false);       // button SUBMIT bisa diklik
                $('#form-info').prop("hidden", false);      // feedback message akan dimunculkan
                $('#form-info').text("Form checks out!");   // feedback message bahwa form sudah valid dan bisa disubmit
            }
        },
    })
}

function submitSubscribe() {
    event.preventDefault(true);
    var full_name = document.getElementById('full_name').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var csrftoken = $('input[name = "csrfmiddlewaretoken"]').attr('value');

    $.ajax({
        method : 'POST',
        url : '/TDD-Registration-submit',
        data : JSON.stringify({'full_name' : full_name, 'email' : email, 'password' : password}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrftoken);
        },
        success : function(response) {
            let formIsPosted = response.form_is_posted;

            if (formIsPosted) {
                $('#submit').prop("disabled", true);
                $('#form-info').prop("hidden", false);
                $('#form-info').text("User subscribed!");
            }
        }
    })
}


// NOTED
// var global
// let local