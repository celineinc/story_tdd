# Import library for unit test
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home, profile, table_books, regist
from django.utils import timezone
from .models import Todo, Subscriber
from .forms import Todo_Form, Subscriber_Form

# Import library for functional test
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

# Create your tests here.
class TDD_Test (TestCase):
    # TEST URL
    def test_TDD_url_is_exist(self):
        response =  Client().get('/TDD-Home')
        self.assertEqual(response.status_code, 200)
    
    # TEST FUNCTION PADA VIEWS.PY
    def test_TDD_using_home_func(self):
        found = resolve('/TDD-Home')
        self.assertEqual(found.func, home)  

    # TEST INSTANSIASI PADA MODEL    
    def test_TDD_model_can_create_new_todo(self):
        # Creating a new activity (sebagai tes)
        new_activity = Todo.objects.create(status='cape woy banyak deadline', date = timezone.now())

        # Retrieving all available activity
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1) # --> 1 karena kan bikin new_activityna cuma 1x 
    
    # TEST INSTANSIASI FORMS.PY
    def test_TDD_form_todo_input_has_placeholder_and_css_classes(self):
        form = Todo_Form()
        self.assertIn('class="form-control-status', form.as_p())
        self.assertIn('id="id_form"', form.as_p())

    # TEST VALIDASI FORM JIKA INPUT KOSONG (Baru dibuat setelah forms.py dibuat hehehe, terlanjur)
    def test_TDD_form_validation_for_blank_items(self):
        form = Todo_Form(data={'status': '', 'date': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]  #---> ini isinya sabeb, ga hrs sesuai dgn error messagenya? soalnya pas di test OK
        )

    # TEST JIKA POST SUKSES DAN DATA YG DI SUBMIT DITAMPILKAN PADA HTML
    # def test_TDD_post_success_and_render_the_result(self):
    #     test = 'Anonymous'
    #     response_post = Client().post('/App_LandingPage/TDD-Home', {'status': test})
    #     self.assertEqual(response_post.status_code, 200)

    #     response= Client().get('/App_LandingPage/TDD-Home') # <--- dari link post_input kok balik ke sini lg?
    #     html_response = response.content.decode('utf8')     # Apa artinya post_input hanya sementara saja, pas data lg di submit?
    #     self.assertIn(test, html_response)
    
    # TEST JIKA POST ERROR SEHINGGA DATA YG DI SUBMIT TIDAK ADA DI HTML
    def test_TDD_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/TDD-Home', {'status': ''})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/TDD-Home')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    # ---------- CHALLENGE -----------

    # TEST URL
    def test_TDD_url_profile_is_exist(self):
        response =  Client().get('/TDD-Profile')
        self.assertEqual(response.status_code, 200)

    # # TEST STORY 8
    # def test_button_change_theme_is_exist(self):
    #     response =  Client().get('/TDD-Home')
    #     html_response = response.content.decode('utf8')
    #     change_button = 
    #     self.assertIn(change_button, html_response)

    # ---------- TEST STORY 9 ----------
    #TEST URL
    def test_TDD_books_url_is_exist(self):
        response =  Client().get('/TDD-Books')
        self.assertEqual(response.status_code, 200)

    # TEST FUNCTION PADA VIEWS.PY
    def test_TDD_books_using_table_books_func(self):
        found = resolve('/TDD-Books')
        self.assertEqual(found.func, table_books)

    # ---------- TEST STORY 10 ----------
    #TEST URL
    def test_TDD_registration_form_url_is_exist(self):
        response =  Client().get('/TDD-Registration')
        self.assertEqual(response.status_code, 200)

    # TEST FUNCTION PADA VIEWS.PY
    def test_TDD_registration_using_regist_func(self):
        found = resolve('/TDD-Registration')
        self.assertEqual(found.func, regist)
    
    # TEST INSTANSIASI PADA MODEL    
    def test_TDD_Registration_can_create_new_subscriber(self):
        # Creating a new activity (sebagai tes)
        new_subscriber = Subscriber.objects.create(full_name='BEBAS', email = 'bebas@gmail.com', password = 'haha')

        # Retrieving all available activity
        counting_all_available_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_available_subscriber, 1)
    
    # TEST INSTANSIASI FORMS.PY
    def test_TDD_Registration_todo_input_has_placeholder_and_css_classes(self):
        form = Subscriber_Form()
        self.assertIn('class="form-control', form.as_p())
        self.assertIn('id="email"', form.as_p())
    
    # TEST VALIDASI FORM YANG BENAR
    def test_TDD_Registration_form_valid_input(self):
        form = Subscriber_Form({'full_name': 'BEBAS', 'email': 'bebas@gmail.com', 'password' : 'hehe'})
        self.assertTrue(form.is_valid())
        
        subscriber = Subscriber()
        subscriber.full_name = form.cleaned_data['full_name']
        subscriber.save()
        self.assertEqual(subscriber.full_name, "BEBAS")

    # TEST VALIDASI FORM JIKA INPUT KOSONG
    def test_TDD_Registration_form_validation_for_blank_items(self):
        form = Subscriber_Form({'full_name': '', 'email': '', 'password' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['full_name'],
            ["This field is required."]  #---> ini isinya sabeb, ga hrs sesuai dgn error messagenya? soalnya pas di test OK
        )

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()
        # self.selenium = webdriver.Chrome()

    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()


    # def test_input_todo(self):
    #     selenium = self.selenium
    #     # Opening the link we want to test
    #     selenium.get("http://story6-tdd.herokuapp.com/TDD-Home")
    #     time.sleep(5)

    #     # Find the form and submit element
    #     form_status = selenium.find_element_by_id('id_form')
    #     submit = selenium.find_element_by_id('submit')

    #     # Fill the form with data
    #     form_status.send_keys('Coba Coba')
    #     time.sleep(1)

    #     # Submit the form
    #     submit.send_keys(Keys.RETURN)
    #     time.sleep(5)

    #     self.assertIn('Coba Coba', selenium.page_source)

    def test_header_title(self):
        self.selenium.get("http://story6-tdd.herokuapp.com/TDD-Home")
        title = self.selenium.find_element_by_id('title').text
        self.assertIn(title, "CELINE SETYAWAN")

    def test_h2(self):
        self.selenium.get("http://story6-tdd.herokuapp.com/TDD-Profile")
        header_text = self.selenium.find_element_by_tag_name('h2').text
        self.assertIn("EXPERIENCES", header_text)

    # # def test_profile_picture(self):
    #     self.selenium.get("http://localhost:8000/TDD-Profile")
    #     picture = self.selenium.find_element_by_class_name('fotoprofil')
    #     self.assertIn()

    def test_home_background_color(self):
        self.selenium.get("http://story6-tdd.herokuapp.com/TDD-Home")
        bg_color = self.selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        self.assertEqual(bg_color, 'rgba(255, 140, 140, 1)')

    def test_submit_button_color(self):
        self.selenium.get("http://story6-tdd.herokuapp.com/TDD-Home")
        button_color = self.selenium.find_element_by_id('submit').value_of_css_property('background-color')
        self.assertEqual(button_color, 'rgba(255, 255, 255, 1)')
    
    # STORY 8
    # def test_clicked_button_change_theme(self):
    #     # Opening the link we want to test
    #     self.selenium.get("http://story6-tdd.herokuapp.com/TDD-Home")
        
    #     # Find the button and click it
    #     theme_button = self.selenium.find_element_by_id('change_theme')
    #     theme_button.send_keys(Keys.RETURN)
    #     time.sleep(3)

    #     menu_color = self.selenium.find_element_by_class_name('navbar-expand-lg').value_of_css_property('background-color')
    #     self.assertEqual(menu_color, 'rgba(255, 255, 0, 1)')
    
    # def test_clicked_accordion(self):
    #     # Opening the link we want to test
    #     self.selenium.get("http://story6-tdd.herokuapp.com/TDD-Profile")
        
    #     # Find the panel title
    #     panel_title = self.selenium.find_element_by_link_text('What is your recent activity?')
    #     time.sleep(1)

    #     # Scroll to where it is located
    #     self.selenium.execute_script("arguments[0].scrollIntoView();", panel_title)
    #     time.sleep(1)

    #     # Click the panel title
    #     panel_title.click()
    #     time.sleep(3)
        
    #     self.assertIn('Trying my best to survive in Fasilkom :)', self.selenium.page_source)

if __name__ == '__main__':
    unittest.main(warnings='ignore')