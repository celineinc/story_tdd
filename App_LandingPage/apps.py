from django.apps import AppConfig


class AppLandingpageConfig(AppConfig):
    name = 'App_LandingPage'
