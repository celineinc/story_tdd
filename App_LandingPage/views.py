from django.shortcuts import render, HttpResponseRedirect
from .forms import Todo_Form, Subscriber_Form
from .models import Todo, Subscriber
import json
from django.http import JsonResponse
import requests

# Create your views here.

def home(request):
    response = {}
    response['author'] = "Chrysant Celine Setyawan"
    response['todo'] = Todo.objects.all()
    response['todo_form'] = Todo_Form
    return render (request, "home_tdd.html", response)

def post_input(request):
    response = {}
    form = Todo_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        todo = Todo(status = response['status'])
        todo.save()
        return HttpResponseRedirect('/TDD-Home')
    else :
        return HttpResponseRedirect('/TDD-Profile')

def profile(request):
    response = {}
    return render (request, "profile.html", response)

def get_json(request):
    json_src = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()
    list_books = []
    for items in json_src['items']:
        books_data = {}
        books_data['title'] = items['volumeInfo']['title']
        books_data['authors'] = items['volumeInfo']['authors']
        # books_data['author'] = []
        # for books_author in items['volumeInfo']['authors']:
        #     books_data['author'].append(books_author)
        books_data['publishedDate'] = items['volumeInfo']['publishedDate']
        books_data['id'] = items['id']

        list_books.append(books_data)
        
    return JsonResponse({'book_data': list_books})

def table_books(request):
    return render(request, 'daftarBuku.html')

# def get_keyword(request, param):
#     url = "https://www.googleapis.com/books/v1/volumes?q=" + param
#     json_src = requests.get(url).json()
#     list_books = []
#     for items in json_src['items']:
#         books_data = {}
#         books_data['title'] = items['volumeInfo']['title']
#         books_data['authors'] = items['volumeInfo']['authors']
#         books_data['publishedDate'] = items['volumeInfo']['publishedDate']
#         books_data['id'] = items['id']

#         list_books.append(books_data)
        
#     return JsonResponse({'book_data': list_books})

def regist(request):
    response = {}
    response['subscriber_form'] = Subscriber_Form
    return render(request, 'registration.html', response)

def subscribe(request):
    if (request.method == 'POST' and request.is_ajax()):
        request = json.loads(request.body)

        subscriber = Subscriber()
        subscriber.full_name = request['full_name']
        subscriber.email = request['email']
        subscriber.password = request['password']                
        subscriber.save()

        return JsonResponse({'form_is_posted' : True})

def check_isValid(request):
    if (request.method == 'POST' and request.is_ajax()):
        request = json.loads(request.body)

        is_exist = False
        is_valid = False

        if (Subscriber.objects.filter(email = request['email']).first()):
            is_exist = True

        form = Subscriber_Form({'full_name': request['full_name'], 'password': request['password'], 'email': request['email']})

        if (form.is_valid()):
            is_valid = True

        return JsonResponse({'email_is_exist' : is_exist, 'form_is_valid': is_valid})



# def books(request):
#     json_src = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
#     json_file = json_src.json()
    # list_books = {
    #     'items' : [],
    #     'addedBooks' : 0,
    # }

    # for data in json_file['items']:
    #     books_data = {}
    #     books_data['title'] = data['volumeInfo']['title']
    #     books_data['author'] = []
    #     for books_author in data['volumeInfo']['authors']:
    #         books_data['author'].append(books_author)
    #     books_data['publishedDate'] = data['volumeInfo']['publishedDate']
    #     list_books['items'].append(books_data)

    # return render (request, "daftarBuku.html", list_books)