from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path("TDD-Home", views.home, name = "TDD-Home"),
    path("TDD-Home-submit", views.post_input, name = "post_input"),
    path("TDD-Profile", views.profile, name = "TDD-Profile"),
    path("TDD-Books", views.table_books, name = "TDD-Books"),
    path("json", views.get_json, name = "get_json"),
    # path("books/<str:param>", views.get_keyword, name = "get_keyword"),
    path("TDD-Registration", views.regist, name = "regist"),
    path("TDD-Registration-submit", views.subscribe, name = "subscribe"),
    path("check-is-valid", views.check_isValid, name = "check_isValid"),
]