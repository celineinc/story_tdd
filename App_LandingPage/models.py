from django.db import models

# Create your models here.class Todo(models.Model)
class Todo(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)

# Models for Subscriber (Lab 10)
class Subscriber(models.Model):
    email = models.EmailField(max_length = 70, default='Input your e-mail here', unique = True)
    full_name = models.CharField(max_length = 70, default='Input your name here')
    password = models.CharField(max_length = 15, default = 'Password must be unique, max. 15 characters')

    # model
    # form
    # html registration (url, views url)
    # views ngecek yg email di databse
    # script
    # views buat submit