from django import forms

#VISUALISASI SCHEDULE FORM

error_messages = {'required' : 'Wajib diisi'}

attrs ={
    'class' : 'form-control-status',
    'type' : 'text',
    'placeholder' : 'Masukkan status Anda di sini',
    'id' : 'id_form'
}

attrs_name ={
    'class' : 'form-control',
    'id' : 'full_name',
    'type' : 'text',
    'placeholder' : 'Input your name here',
    'oninput' : 'checkRegistered()',
}

attrs_email ={
    'class' : 'form-control',
    'id' : 'email',
    'type' : 'email',
    'placeholder' : 'Input your e-mail here',
    'oninput' : 'checkRegistered()',
}

attrs_pass ={
    'class' : 'form-control',
    'id' : 'password',
    'placeholder' : 'Input your password here',
    'oninput' : 'checkRegistered()',
}

class Todo_Form(forms.Form):
    status = forms.CharField(label = "Status", required=True, max_length=300, empty_value="", widget = forms.TextInput(attrs= attrs))

class Subscriber_Form(forms.Form):
    full_name = forms.CharField(label = "Full Name", required=True, max_length=70, empty_value="", widget = forms.TextInput(attrs= attrs_name))
    email = forms.EmailField(label = "E-mail", required=True, max_length=30, empty_value="", widget = forms.TextInput(attrs= attrs_email))
    password = forms.CharField(label = "Password", required = True, max_length=30, empty_value="", widget = forms.PasswordInput(attrs= attrs_pass))