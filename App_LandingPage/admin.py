from django.contrib import admin
from .models import Todo, Subscriber

# Register your models here.
admin.site.register(Todo)
admin.site.register(Subscriber)